#include <Arduino.h>
#include <AccelStepper.h>
#include <Servo.h>


AccelStepper motor1(AccelStepper::DRIVER, 24, 27);
AccelStepper motor2(AccelStepper::DRIVER, 23, 26);
AccelStepper motor3(AccelStepper::DRIVER, 22, 25);

Servo pinch;
Servo twist;
Servo wrist;

int sign = 1;      // Either 1, 0 or -1
int spd = 4000;    // The current speed in steps/second

// Set motors 4-6 to home position
float motor4 = 90.0;
float motor5 = 90.0;
float motor6 = 90.0;

void setup() {
  Serial.begin(9600);

  
  //set pins

  pinMode(30, INPUT);
  pinMode(31, INPUT);
  pinMode(28, INPUT);
  pinMode(29, INPUT);
  pinMode(38, INPUT);
  pinMode(39, INPUT);
  pinMode(36, INPUT);
  pinMode(37, INPUT);
  pinMode(34, INPUT);
  pinMode(35, INPUT);

  digitalWrite(30, LOW);
  digitalWrite(31, LOW);
  
  motor1.setMaxSpeed(4000);
  motor1.setSpeed(4000);   
  motor2.setMaxSpeed(4000);
  motor2.setSpeed(4000);   
  motor3.setMaxSpeed(4000);
  motor3.setSpeed(4000);   

  pinch.attach(48);
  twist.attach(47);
  wrist.attach(51);
  //set stepper motor acceleration and max speed
  //motor1.setAcceleration(100.0);
}

void loop() {
  if (digitalRead(30) == HIGH) {
   motor1.setSpeed(-1 * spd);
  }
  else if (digitalRead(31) == HIGH){
   motor1.setSpeed(sign * spd);
  }
  else{
    digitalWrite(30, LOW);
    digitalWrite(31, LOW);
    motor1.setSpeed(0);
  }



  if (digitalRead(29) == HIGH) {
   motor2.setSpeed(-1 * spd);
  }
  else if (digitalRead(28) == HIGH){
   motor2.setSpeed(sign * spd);
  }
  else{
    digitalWrite(28, LOW);
    digitalWrite(29, LOW);
    motor2.setSpeed(0);
  }



  if (digitalRead(32) == HIGH) {
   motor3.setSpeed(-1 * 3500);
  }
  else if (digitalRead(33) == HIGH){
   motor3.setSpeed(sign * 3500);
  }
  else{
    digitalWrite(32, LOW);
    digitalWrite(33, LOW);
    motor3.setSpeed(0);
  }




  if (digitalRead(38) == HIGH) {
   if (motor4 < 120){
    motor4 += 0.02;
   }
  }
  else if (digitalRead(39) == HIGH) {
   if (motor4 > 50){
    motor4 -= 0.02;
   }
  }

  if (digitalRead(36) == HIGH) {
   if (motor5 < 180){
    motor5+=0.02;
   }
  }
  else if (digitalRead(37) == HIGH) {
   if (motor5 > 0){
    motor5-=0.02;
   }
  }
  if (digitalRead(34) == HIGH) {
   if (motor6 < 180){
    motor6+=0.02;
   }
  }
  else if (digitalRead(35) == HIGH) {
   if (motor6 > 0){
    motor6-=0.02;
   }
  }
  motor1.runSpeed();
  motor2.runSpeed();
  motor3.runSpeed();


  pinch.write(motor4);
  twist.write(motor5);
  wrist.write(motor6);
//  delay(2000);
//  pinch.write(50);
//  delay(2000);
//  twist.write(1);
//  delay(2000);
//  pinch.write(120);
//  delay(2000);
//  twist.write(180);
//  delay(2000);
//  wrist.write(180);
//  delay(2000);
  
}