import cv2
import serial

#240, 320, 3
cap = cv2.VideoCapture(0)

ser = serial.Serial('COM4', 115200, timeout=1)

# Check if the webcam is opened correctly
if not cap.isOpened():
    raise IOError("Cannot open webcam")

while True:
    ret, frame = cap.read()
    frame = cv2.resize(frame, None, fx=0.5, fy=0.5, interpolation=cv2.INTER_AREA)
    cv2.imshow('Input', frame)

    #print(frame[1,1] , " ----------- " , frame[1,318])
    #print(frame[1,1][2])
    if (frame[1,1][0] <= 100 and frame[1,1][1] <= 100 and frame[1,1][2] <= 100):
        print("Right!")
        ser.write(b'a')
    elif (frame[1,318][0] <= 150 and frame[1,318][1] <= 150 and frame[1,318][2] <= 150):
        print("Left!")
        ser.write(b'b')

    c = cv2.waitKey(1)
    if c == 27:
        #cv2.imwrite("frame%d.jpg", frame)     # save frame as JPEG file
        break

cap.release()
cv2.destroyAllWindows()
ser.close()

